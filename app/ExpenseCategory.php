<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseCategory extends Model
{
    protected $table = 'expense_category';
    protected $guarded = [];

    public $timestamps = true;

    public function expenses()
    {
        return $this->belongsTo('App\Expenses', 'id', 'expense_category_id');
    }
}
