<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model
{
    protected $table = 'expenses';
    protected $guarded = [];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\User', 'id', 'role_id');
    }

    public function expense_category()
    {
        return $this->hasOne('App\ExpenseCategory', 'id', 'expense_category_id');
    }
}
