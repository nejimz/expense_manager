<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExpenseCategory;

class ExpensesCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 100;
        if ($request->has('page')) {
            $page = trim($request->page);
        }
        $rows = ExpenseCategory::paginate(100);
        return response()->json($rows);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:expense_category,name',
            'description' => 'required'
        ]);

        $row = ExpenseCategory::create($request->only('name', 'description'));

        return response()->json(['message'=>'Expense Category Successfully Added!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = ExpenseCategory::whereId($id)->first();

        if (is_null($row)) {
            return response()->json(['message' => 'Expense Category not found!']);
        }

        return response()->json($row);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:expense_category,name,' . $request->id,
            'description' => 'required'
        ]);

        $row = ExpenseCategory::whereId($request->id)->update($request->only('name', 'description'));

        return response()->json(['message'=>'Expense Category Successfully Updated!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $row = ExpenseCategory::whereId($id)->delete();
        return response()->json(['message' => 'Expense Category succesfully deleted!']);
    }
}
