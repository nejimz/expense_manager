<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expenses;
use App\ExpenseCategory;
use App\Http\Resources\ExpensesResource;

class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = 100;
        if ($request->has('page')) {
            $page = trim($request->page);
        }
        $rows = Expenses::with('expense_category')->paginate(100);
        return ExpensesResource::collection($rows)->response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required|exists:users,id',
            'expense_category_id' => 'required',
            'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'entry_date' => 'required|date'
        ]);

        $row = Expenses::create($request->only('user_id', 'expense_category_id', 'amount', 'entry_date'));

        return response()->json(['message'=>'Expenses Successfully Added!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Expenses::whereId($id)->first();

        if (is_null($row)) {
            return response()->json(['message' => 'Expenses not found!']);
        }

        return response()->json($row);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'user_id' => 'required|exists:users,id',
            'expense_category_id' => 'required',
            'amount' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'entry_date' => 'required|date'
        ]);

        $row = Expenses::whereId($request->id)->update($request->only('user_id', 'expense_category_id', 'amount', 'entry_date'));

        return response()->json(['message'=>'Expenses Successfully Updated!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $row = Expenses::whereId($id)->delete();
        return response()->json(['message' => 'Expenses succesfully deleted!']);
    }

    public function chart_data($id)
    {
        $counter = 0;
        $data = [];
        #$rows = Expenses::whereUserId($id)->get();
        $rows = ExpenseCategory::get();
        foreach ($rows as $row) {
            $total_amount = Expenses::whereUserId($id)->whereExpenseCategoryId($row->id)->sum('amount');
            $data[$counter] = [
                'name' => $row->name, 
                'y' => $total_amount
            ];
            $counter++;
        }
        return response()->json($data);
    }
}
