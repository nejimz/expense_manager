<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExpensesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        #return parent::toArray($request);
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'expense_category_id' => $this->expense_category_id,
            'amount' => number_format($this->amount, 2),
            'expense_category_name' => $this->expense_category->name,
            'entry_date' => $this->entry_date,
            'created_at' => $this->created_at->format('Y-m-d')
        ];
    }
}
