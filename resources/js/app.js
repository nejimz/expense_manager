
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import HighchartsVue from 'highcharts-vue'
import router from './router'
import {store} from './store/store'
import App from './components/App'

require('./bootstrap');

/*const app = new Vue({
    el: '#app',
    components: {
        App
    },
    router
});*/
const app = new Vue({
    router,
    store: store,
    render: h => h(App),
    el: '#app'
    /*components: {
        App
    },*/
});
