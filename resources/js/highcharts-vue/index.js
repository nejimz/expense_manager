import Vue from 'vue'
import HighchartsVue from 'highcharts-vue'

Vue.use(HighchartsVue)

const opts = {}

export default new HighchartsVue(opts)