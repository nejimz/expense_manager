import Vue from 'vue'
import VueRouter from 'vue-router'
// components
import App from './components/App'  
import Login from './components/Login'
import Home from './components/Home'

import Users from './components/Users'
import Roles from './components/Roles'
import ExpenseCategory from './components/ExpenseCategory'
import Expenses from './components/Expenses'

import ChangePassword from './components/ChangePassword'
import Logout from './components/Logout'

//import store from '@/store'

Vue.use(VueRouter);

//export default new VueRouter({
const router = new VueRouter({
	routes: [
		{
			path: '/', 
			name: 'app', 
			component: App,
			redirect: '/login'
		},
		{
			path: '/login', 
			name: 'login', 
			component: Login 
		},
		{
			path: '/home', 
			name: 'home', 
			component: Home,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/users', 
			name: 'users', 
			component: Users,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/roles/', 
			name: 'roles', 
			component: Roles,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/expense-category', 
			name: 'expense-category', 
			component: ExpenseCategory,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/expenses', 
			name: 'expenses', 
			component: Expenses,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/change-password', 
			name: 'change-password', 
			component: ChangePassword,
			meta: {
				requiresAuth: true
			}
		},
		{
			path: '/logout', 
			name: 'logout', 
			component: Logout,
			meta: {
				requiresAuth: true
			}
		}
	],
	mode: 'history'
})


router.beforeEach((to, from, next) => {
	if(to.meta.requiresAuth) {
		if(localStorage.getItem( 'user_name' )) {
			//console.log(to.matched.some());
			var url_list = ['roles', 'users', 'expense-category'];
			if (url_list.indexOf(to.matched[0]['name']) >= 0 && localStorage.getItem( 'user_role' ) != 1) {
				next({name: 'home'});
			}
			next();
		} else {
			next('/login');
		}
	} else {
		next();
	}
})

export default router