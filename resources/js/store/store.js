import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
	state: {
		fullName: '',
		isAdmin: false
	},

	getters: {
		isAdmin(state) {
			return state.isAdmin
		},

		fullName (state) {
			return state.fullName
		}
	},

	mutations: {
		setIsAdmin (state, value) {
			state.isAdmin = value
		},

		fullName (state, value) {
			state.fullName = value
		}
	}
})