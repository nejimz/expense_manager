<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('users', 'UsersController')->except(['create', 'edit']);
Route::resource('roles', 'RolesController')->except(['create', 'edit']);
Route::resource('expense-category', 'ExpensesCategoryController')->except(['create', 'edit']);
Route::resource('expenses', 'ExpensesController')->except(['create', 'edit']);
Route::get('data-chart/{id}', 'ExpensesController@chart_data')->name('expenses.data-chart');
Route::post('change-password', 'UsersController@change_password')->name('users.change-password');

Route::post('authentication', 'LoginController@authentication')->name('login.authentication');
