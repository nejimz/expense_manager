<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/{any?}', 'LoginController@index')->where('any',  '.*')->name('login.index');
#Route::post('/{any?}', 'LoginController@index')->where('any',  '(?!storage).*$')->name('login.post');
#Route::post('/{any?}', 'LoginController@index')->where('any',  '^(?!api).*$')->name('login.index');